let restartMenu = new Phaser.Scene('restartMenu');
restartMenu.preload=function preload ()
{
    this.load.setBaseURL('assets');
    this.load.image('pagain','playagain.jpg');
}
var scoreText;
restartMenu.create=function create ()
{
    console.log("msk");
    this.add.image(config.width/2,config.height/2,'pagain');
    var btn = this.add.text(config.width/2-200, config.height, 'YES', { fontSize: '32px', fill: '#fff' });
    var btnNo = this.add.text(config.width/2+200, config.height, 'NO', { fontSize: '32px', fill: '#fff' });
    btn.scaleX=0;
    btn.scaleY=0;
    btn.setInteractive();
    btnNo.scaleX=0;
    btnNo.scaleY=0;
    btnNo.setInteractive();
    btn.on('pointerup',function(){
        this.scene.scene.start('main');
    });

    btnNo.on('pointerup',function(){
        this.scene.scene.start('menu');
    });


    this.tweens.add({
        targets:[btn],
        y:config.height/2+150,
        duration:500,
        scaleX:1,
        scaleY:1,
        ease: 'Bounce.easeOut'
    });

    this.tweens.add({
        targets:[btnNo],
        y:config.height/2+150,
        duration:500,
        scaleX:1,
        scaleY:1,
        ease: 'Bounce.easeOut'
    });
}
