
let main = new Phaser.Scene('main');
main.preload=function(){
    this.load.setBaseURL('assets');
    this.load.image('tile','tile.png');
    //this.load.atlas('pohon','tile2.png','pohon.json');
    this.load.image('burger','burger.png');
    this.load.image('noodle','noodle.png');
    this.load.image('toast','toast.png');
    this.load.image('sushi','sushi.png');
    this.load.atlas('character','walkleft.png','walkleft.json');
    this.load.atlas('character2','walkright.png','walkright.json');
    this.load.image('table','table.png');
    this.load.image('chatbox','chatbox.png');
    this.load.image('chatboxInv','chatboxInv.png');

}
var idxFood=0;
var score=0;
var queueIdx=[];
var foodArr=['burger','toast','sushi','noodle'];
var foodReq=['','','',''];
var foodqueue=[];
var walker;
var walker2;
var velo=200;

var tbl4;
var tbl3;
var tbl2;
var tbl1;

main.create=function(){
    start=true;
    this.tiles=this.add.group();
    this.foods=this.add.group();
    
    walker=this.physics.add.sprite(config.width/2+350,config.height/2+200,'character','char01L.png');
    walker.body.setGravityY(0);
    walker.setScale(0.2);

    walker2=this.physics.add.sprite(config.width/2-350,config.height/2+200,'character2','char1R.png');
    walker2.body.setGravityY(0);
    walker2.setScale(2.5);

    walker3=this.physics.add.sprite(config.width/2+350,config.height/2-200,'character2','char1R.png');
    walker3.body.setGravityY(0);
    walker3.setScale(0.2);

    walker4=this.physics.add.sprite(config.width/2-350,config.height/2-200,'character2','char1R.png');
    walker4.body.setGravityY(0);
    walker4.setScale(2.5);

    this.generateDialogue(1);
    this.generateDialogue(2);
    this.generateDialogue(3);
    this.generateDialogue(4);
    this.anims.create(
        {key:'walking',
        frames:this.anims.generateFrameNames('character',{start:1,end:3,zeroPad:2,prefix:'char',suffix:'L.png'}),
        repeat:-1,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'walking2',
        frames:this.anims.generateFrameNames('character2',{start:1,end:3,zeroPad:0,prefix:'char',suffix:'R.png'}),
        repeat:-1,
        frameRate:10
        }
    );
    tbl1=this.generateTable(config.width/2+200,config.height/2+200);
    tbl2=this.generateTable(config.width/2-200,config.height/2+200);
    tbl3=this.generateTable(config.width/2+200,config.height/2-200);
    tbl4=this.generateTable(config.width/2-200,config.height/2-200);

    tbl1.setInteractive();
    tbl2.setInteractive();
    tbl3.setInteractive();
    tbl4.setInteractive();
    tbl1.on('pointerup',()=>{
        console.log('1');
        console.log(this.foods);
        if(foodqueue[0]!=foodReq[0]){
            this.restart();
        }
        else{
            score++;
            foodqueue.shift();
            var tempx=tbl1.x-this.foods.getChildren()[idxFood].x;
            var tempy=tbl1.y-this.foods.getChildren()[idxFood].y;
            this.foods.getChildren()[idxFood].setVelocity(tempx,tempy);
            idxFood++;
            this.generateDialogue(1);
        }
    });
    tbl2.on('pointerup',()=>{
        console.log('2');
        if(this.foods.getLength()>0){
            if(foodqueue[0]!=foodReq[1]){
                this.restart();
            }
            else{
                
                score++;
                foodqueue.shift();
                var tempx=tbl2.x-this.foods.getChildren()[idxFood].x;
                var tempy=tbl2.y-this.foods.getChildren()[idxFood].y;
                this.foods.getChildren()[idxFood].setVelocity(tempx,tempy);
                t2Idx=idxFood;
                idxFood++;
                this.generateDialogue(2);
            }
        }
    });
    tbl3.on('pointerup',()=>{
        console.log('3');
        if(this.foods.getLength()>0){
            if(foodqueue[0]!=foodReq[2]){
                this.restart();
            }
            else{
                
                score++;
                foodqueue.shift();
                var tempx=tbl3.x-this.foods.getChildren()[idxFood].x;
                var tempy=tbl3.y-this.foods.getChildren()[idxFood].y;
                this.foods.getChildren()[idxFood].setVelocity(tempx,tempy);
                t3Idx=idxFood;
                idxFood++;
                this.generateDialogue(3);
            }
        }
    });
    tbl4.on('pointerup',()=>{
        console.log('4');
        if(this.foods.getLength()>0){
            if(foodqueue[0]!=foodReq[3]){
                this.restart();
            }
            else{
                
                score++;
                foodqueue.shift();
                var tempx=tbl4.x-this.foods.getChildren()[idxFood].x;
                var tempy=tbl4.y-this.foods.getChildren()[idxFood].y;
                this.foods.getChildren()[idxFood].setVelocity(tempx/2,tempy/2);
                t4Idx=idxFood;
                idxFood++;
                this.generateDialogue(4);
            }
        }
    });
    walker.play('walking');
    walker2.play('walking2');
    walker3.play('walking');
    walker4.play('walking2');
    for(i=0;i<4;i++){
         this.generateTile(config.width/2,(260*i));
         queueIdx.push(3-i);
    }
    this.timer=this.time.addEvent({
        delay:1500,
        callback:()=>{
            if(start){
                var i=Math.floor(Math.random() * 4);
                velo++;
                this.generateFood(config.width/2,0,foodArr[i]);
                foodqueue.push(foodArr[i]);
            }  
            },
        callbackScope:this,
        loop:true
    });
}


main.update=function(){
    if(start){
        if(this.tiles.getLength()>0 && this.tiles.getChildren()[queueIdx[1]].y>config.height){
            //console.log('msk');
            var idx=queueIdx.shift();
            this.tiles.getChildren()[idx].y=this.tiles.getChildren()[queueIdx[2]].y-260;
            queueIdx.push(idx);
        }
        console.log(idxFood);
        if(this.foods.getLength()>0 && this.foods.getChildren()[idxFood].y>config.height){
            //console.log(idxFood);
            var food=foodqueue.shift();
            console.log(food,foodReq[0],foodReq[1],foodReq[2],foodReq[3]);
            if(food==foodReq[0])this.restart();
            else if(food==foodReq[1])this.restart();
            else if(food==foodReq[2])this.restart();
            else if(food==foodReq[3])this.restart();
            if(start)idxFood++;
        }
    }
    
}


main.generateTile=function(x,y){
    var tile=this.physics.add.sprite(x,y,'tile');
    tile.setActive();
    tile.setVelocity(0,velo);
    tile.setGravity(0);
    tile.setScale(1);
    this.tiles.add(tile);
}

main.generateFood=function(x,y,str){
    var food=this.physics.add.sprite(x,y,str);
    food.setActive();
    food.setVelocity(0,velo);
    food.setGravity(0);
    if(str=='burger')food.setScale(3);
    else if(str=='toast')food.setScale(0.3);
    else if(str=='sushi')food.setScale(0.6);
    else food.setScale(0.3);
    this.foods.add(food);
}

main.generateTable=function(x,y){
    var table=this.physics.add.sprite(x,y,'table');
    table.setActive();
    table.setScale(0.6);
    return table;
    //table = this.physics.add.staticGroup();
}

var temp=[[],[],[],[]];
main.generateDialogue=function(table){
    var strbox;
    var x=config.width/2;
    var y=config.width/2;
    if(table==1){
        x+=350;
        strbox='chatbox';
    }
    else if(table==2){
        x-=350;
        strbox='chatbox';
    }
    else if(table==3){
        x+=350;
        y-=150;
        strbox='chatboxInv';
    }
    else{
        x-=350;
        y-=150;
        strbox='chatboxInv';
    }
    if(temp[table-1][0]!=null){
        //temp[table-1][0].destroy();
        //temp[table-1][1].destroy();
    }
    var box=this.add.image(x,y,strbox);
    var i=Math.floor(Math.random() * 4);
    var str=foodArr[i];
    foodReq[table-1]=str;
    var food=this.add.image(x,y,str);
    temp[table-1][0]=box;
    temp[table-1][1]=food;
    box.setScale(0.5);
    if(str=='burger')food.setScale(2);
    else if(str=='toast')food.setScale(0.18);
    else if(str=='sushi')food.setScale(0.4);
    else food.setScale(0.1);
    
}
main.restart=function(){
    this.scene.pause();
    start=false;
    idxFood=0;
    this.foods.clear();
    foodqueue=[];
    this.tiles.clear();
    score=0;
    queueIdx=[];
    this.scene.start('restartMenu');
}
